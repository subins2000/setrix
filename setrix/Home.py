#!/usr/bin/python3

'''
Setrix
Copyleft Subin Siby
Licensed under GNU-GPL v3.0
'''

import datetime
import json
import os
import subprocess
import sys
from urllib.parse import urlparse

from PyQt5 import uic, QtCore
from PyQt5.QtCore import Qt, QSettings
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QMainWindow, QWidget, QAction, qApp, QStyle, QDesktopWidget, QListWidget, QListWidgetItem, QPushButton, QLineEdit, QTextBrowser, QStatusBar, QMessageBox, QFileDialog

from matrix_client.client import MatrixClient, Room
from matrix_client.api import MatrixHttpApi, MatrixRequestError
from requests.exceptions import MissingSchema, InvalidSchema

from setrix.ui import *
from setrix.MatrixWorker import *

class Home(QMainWindow):

    matrixClient = matrixAPI = None

    homeserver = userID = msgAttachedFile = None

    rooms = []
    threads = []

    def __init__(self):
        super().__init__()

        self.initUI()
        self.initMenubar()
        self.initIntro()

    def initUI(self):
        self.ui = Ui_Home()
        self.ui.setupUi(self)

        self.setGeometry(
            QStyle.alignedRect(
                Qt.LeftToRight,
                Qt.AlignCenter,
                self.size(),
                qApp.desktop().availableGeometry()
            )
        )
        self.show()

    def initMenubar(self):
        self.ui.quitAction.triggered.connect(qApp.quit)

    def initIntro(self):
        settings = QSettings('setrix', 'setrix')
        token = settings.value('token')

        if token:
            self.showChat()
        else:
            self.showLogin()

    def statusMSG(self, msg = ''):
        self.statusBar().showMessage(msg)

    def startMatrixThread(self, action, arguments = {}):
        worker = MatrixWorker(action, self.matrixClient, self.matrixAPI, arguments)

        thread = QThread()
        self.threads.append((thread, worker))  # need to store worker too otherwise will be gc'd

        worker.moveToThread(thread)
        worker.finished.connect(self.onMatrixThreadResponse)

        thread.started.connect(worker.run)
        thread.start()

    def showError(self, heading, body):
        self.statusMSG(body)
        QMessageBox.about(self, heading, body)

    def onMatrixThreadResponse(self, action, result):
        if action == 'startClient':

            self.matrixClient = result

            settings = QSettings('setrix', 'rooms')
            self.rooms = json.loads(settings.value('rooms', '[]'))

            if self.rooms:
                self.updateRoomList(True)

            self.startMatrixThread('getRooms', {
                'rooms': self.rooms
            })

            self.matrixClient.add_listener(self.matrixEventListener)
            self.matrixClient.start_listener_thread()

        elif action == 'login':

            if 'error' in result:
                error = result['error']
                self.showError(
                    "Error",
                    "Error %s : %s" % (error['errcode'], error['error'])
                )
            else:
                settings = QSettings('setrix', 'setrix')
                settings.setValue('homeserver', result['homeserver'])
                settings.setValue('token', result['token'])
                settings.setValue('userID', result['userID'])
                del settings

                self.statusMSG()
                self.showChat()

        elif action == 'getRooms':
            if not result:
                return

            self.rooms, newRooms = result

            settings = QSettings('setrix', 'rooms')
            settings.setValue('rooms', json.dumps(self.rooms))
            del settings

            self.updateRoomList(False, newRooms)

        elif action == 'getMessages':
            self.ui.msgs.setText('')

            for msg in reversed(result):
                self.processEvent(msg)

            self.statusMSG('')

    def showLogin(self):
        self.ui.logOutAction.setVisible(False)
        self.ui.chatWidget.hide()
        self.ui.roomList.clear()
        self.ui.roomList.hide()

        self.ui.password.setEchoMode(QLineEdit.Password)
        self.ui.loginButton.clicked.connect(self.onLogin)
        self.ui.username.returnPressed.connect(self.onLogin)
        self.ui.password.returnPressed.connect(self.onLogin)

        self.ui.loginWidget.show()

    def onLogin(self):
        homeserver = self.ui.homeserver.text()
        username = self.ui.username.text()
        password = self.ui.password.text()

        self.statusMSG('Logging In')

        self.startMatrixThread(
            'login', {
                'homeserver': homeserver,
                'username': username,
                'password': password
            }
        )

    def showChat(self):
        settings = QSettings('setrix', 'setrix')

        self.ui.loginWidget.hide()
        self.ui.chatWidget.show()
        self.ui.roomList.show()

        self.homeserver = settings.value('homeserver')
        self.userID = settings.value('userID')

        self.startMatrixThread('startClient', {
            'settings': settings
        })

        self.matrixAPI = MatrixHttpApi(
            self.homeserver,
            token=settings.value('token')
        )

        self.ui.logOutAction.setVisible(True)
        self.ui.logOutAction.triggered.connect(self.doLogOut)
        self.ui.roomList.itemClicked.connect(self.onRoomChange)
        self.ui.sendButton.clicked.connect(self.onSendButtonClick)
        self.ui.sendMSGText.returnPressed.connect(self.onSendButtonClick)
        self.ui.attachFileButton.clicked.connect(self.onAttachFileButtonClick)

    def getCurrentActiveRoom(self):
        selectedRoomIndex = self.ui.roomList.currentRow()
        return self.rooms[selectedRoomIndex]

    def onRoomChange(self):
        self.statusMSG('Retrieving Messages')

        self.startMatrixThread('getMessages', {
            'roomID': self.getCurrentActiveRoom()['roomID'],
            'token': self.matrixClient.get_sync_token(),
            'type': 'recent'
        })

    def processEvent(self, msg):
        user = msg['sender']
        msgtype = msg['type']

        appendMSG = False

        if msgtype == 'm.room.message':

            if msg['content']['msgtype'] == 'm.text':
                appendMSG = '<i>' + user + '</i> => ' + msg['content']['body']
            elif msg['content']['msgtype'] == 'm.image':

                urlParts = urlparse(msg['content']['url'])

                if 'h' in msg['content']['info']:
                    height = str(msg['content']['info']['h'])
                    width = str(msg['content']['info']['w'])

                    thumbnailURL = self.homeserver + '/_matrix/media/r0/thumbnail/{serverName}{mediaID}?height={height}&width={width}'.format(
                        serverName=urlParts.netloc,
                        mediaID=urlParts.path,
                        height=height,
                        width=width
                    )

                    appendMSG = '<i>' + user + '</i> => <a href="' + thumbnailURL + '"><img src="' + thumbnailURL + '" /> Image</a>'
                else:
                    downloadURL = self.homeserver + '/_matrix/media/r0/download/{serverName}{mediaID}'.format(
                        serverName=urlParts.netloc,
                        mediaID=urlParts.path
                    )

                    appendMSG = '<i>' + user + '</i> => <a href="' + downloadURL + '"><img src="' + downloadURL + '" /> Image</a>'

            elif msg['content']['msgtype'] == '':
                urlParts = urlparse(msg['content']['url'])

                downloadURL = self.homeserver + '/_matrix/media/r0/download/{serverName}{mediaID}'.format(
                    serverName=urlParts.netloc,
                    mediaID=urlParts.path
                )

                if msg['content']['body'] == '':
                    downloadText = 'Download Attachment'
                else:
                    downloadText = 'Download ' + msg['content']['body']

                appendMSG = '<i>' + user + '</i> => <a href="' + downloadURL + '">' + downloadText + '</a>'

        elif msgtype == 'm.room.member':

            appendMSG = '<i>' + user + '</i>'

            if msg['membership'] == 'join':
                appendMSG += ' joined the room'

            if msg['membership'] == 'leave':
                appendMSG += ' left the room'

        if appendMSG:
            if self.ui.msgs.text() == '':
                currentContent = ''
            else:
                currentContent = self.ui.msgs.text() + '<br>'

            self.ui.msgs.setText(currentContent + appendMSG)

    def onSendButtonClick(self):
        msg = self.ui.sendMSGText.text()

        if self.msgAttachedFile:
            self.startMatrixThread('sendMessage', {
                'roomID': self.getCurrentActiveRoom()['roomID'],
                'type': 'img',
                'msg': msg,
                'location': self.msgAttachedFile
            })

            self.msgAttachedFile = None
            self.ui.attachFileButton.setStyleSheet("background-color: gray")

        else:
            self.startMatrixThread('sendMessage', {
                'roomID': self.getCurrentActiveRoom()['roomID'],
                'type': 'text',
                'msg': msg
            })

        self.processEvent({
            'sender': self.userID,
            'type': 'm.text',
            'content': {
                'msgtype': 'm.txt',
                'body': msg
            }
        })

        self.ui.sendMSGText.setText('')

    def onAttachFileButtonClick(self):
        fileLocation, _ = QFileDialog.getOpenFileName(
            self,
            'Open File'
        )

        if fileLocation:
            self.msgAttachedFile = fileLocation
            self.ui.attachFileButton.setStyleSheet("background-color: blue")
        else:
            self.msgAttachedFile = None
            self.ui.attachFileButton.setStyleSheet("background-color: gray")

    def doLogOut(self):
        settings = QSettings('setrix', 'setrix')
        settings.clear()

        settings = QSettings('setrix', 'rooms')
        settings.clear()

        self.showLogin()

    def updateRoomList(self, selectFirst = False, newRooms = []):
        if newRooms:
            for v in newRooms:
                self.ui.roomList.addItems([v['name']])
        else:
            self.ui.roomList.clear()

            for v in self.rooms:
                self.ui.roomList.addItems([v['name']])

        if selectFirst:
            self.ui.roomList.setCurrentRow(0)
            self.onRoomChange()

    def matrixEventListener(self, data):
        self.processEvent(data)
