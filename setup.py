#!/usr/bin/env python3

from setuptools import find_packages, setup

setup(
    name='Setrix',
    version='0.1',
    url='https://gitlab.com/subins2000/setrix',
    author='Subin Siby',
    author_email='subins2000@gmail.com',
    description=('A matrix client.'),
    license='GPL',
    install_requires=[
        'matrix_client',
        'Pillow'
    ],
    packages=find_packages(exclude=['designer']),
    entry_points={'gui_scripts': [
        'setrix = setrix.setrix:main'
    ]},
)
