# Setrix

A desktop matrix client

## Requirements

* python3
    * PyQt5
    * matrix_client
    * Pillow

Requirements can be installed by :

```
pip3 install PyQt5 --user && pip3 install matrix_client --user && pip3 install Pillow
```

## Installation

```
python3 setup.py install
```

## Running

You can directly run without installing by :

```
python3 setrix.py
```

Or after installation, run by :

```
setrix
```
